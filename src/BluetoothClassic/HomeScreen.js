import { View, Text, Button } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native';

export default function HomeScreen() {
  const navigation = useNavigation(); 
  return (
    <View style={{flex:1, justifyContent:'space-around', alignSelf: 'center'}}>
      <Button title='Bluetooth Chat Serial' onPress={() => navigation.navigate('BleChatSerial')}/>
      <Button title='Bluetooth chat Classic' onPress={() => navigation.navigate('Main')}/>
      <Button title='Bluetooth example' onPress={() => navigation.navigate('SecondExample')}/>
      <Button title='Chat demo' onPress={() => navigation.navigate('ChatDemo')}/>
    </View>
  )
}