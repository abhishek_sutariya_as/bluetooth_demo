import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from './BluetoothClassic/HomeScreen';
import BleChatSerial from './Chat/BleChatSerial';
import Main from './BluetoothClassic/Main';
import SecondExample from '../SecondExample';
import ChatDemo from './Chat/ChatDemo';
import DeviceListScreen from './BluetoothClassic/device-list/DeviceListScreen';
import ConnectionScreen from './BluetoothClassic/connection/ConnectionScreen';

const Stack = createNativeStackNavigator();

export default routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="HomeScreen"
          component={HomeScreen}
          options={{title: 'HomeScreen'}}
        />
        <Stack.Screen
          name="BleChatSerial"
          component={BleChatSerial}
          options={{title: 'BleChatSerial'}}
        />
        <Stack.Screen name="Main" component={Main} options={{title: 'Main'}} />
        <Stack.Screen
          name="SecondExample"
          component={SecondExample}
          options={{title: 'SecondExample'}}
        />
        <Stack.Screen
          name="ChatDemo"
          component={ChatDemo}
          options={{title: 'ChatDemo'}}
        />
        <Stack.Screen
          name="DeviceListScreen"
          component={DeviceListScreen}
          options={{title: 'DeviceListScreen'}}
        />
        <Stack.Screen
          name="ConnectionScreen"
          component={ConnectionScreen}
          options={{title: 'ConnectionScreen'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
