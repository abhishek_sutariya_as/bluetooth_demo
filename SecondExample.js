// import React, { useState } from "react";
// import { Container, Header, Content, Tab, Tabs, Text, Button } from 'react-native';
// import { Col, Row, Grid } from 'react-native-easy-grid';
// import { View, Alert, NativeAppEventEmitter, NativeModules, NativeEventEmitter } from 'react-native';
// import BleManager from 'react-native-ble-manager';
// import { useEffect } from "react";

// const BleManagerModule = NativeModules.BleManager;
// const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

// function SecondExample({ navigation }) {
// const [peripherals, setPeripherals] = useState([])

// useEffect(() => {

//     BleManager.start({ showAlert: false })
//         .then(() => {
//             console.log('BleManager initialized');

//             bleManagerEmitter.addListener(
//                 'BleManagerDiscoverPeripheral',
//                 (args) => {
//                     console.log(args);

//                     if (args.name !== null) {
//                         var peripheral = { 'id': args.id, 'name': args.name };
//                         peripherals.push(peripheral);

//                         var newArr = []
//                         for (var i = 0; i < peripherals.length; i++) {
//                             if (newArr.indexOf(peripherals[i]) === -1) {
//                                 newArr.push(peripherals[i])
//                                 setPeripherals(newArr);
//                             }
//                         }
//                     }

//                 }
//             );

//             BleManager.checkState();
//         })
//         .catch((error) => {
//             console.log(error);
//         });

// }, [])

// const scanForDevices = () => {
//     BleManager.scan([], 5, false)
//         .then((args) => {
//             console.log(args);
//             console.log('Scan started');
//             setPeripherals([]);
//         })
//         .catch((error) => {
//             console.log(error);
//         });

// }

// // const viewDevice = () => {
// //     BleManager.getBondedPeripherals([]).then((bondedPeripheralsArray) => {
// //         // Each peripheral in returned array will have id and name properties
// //         console.log(bondedPeripheralsArray);
// //         setPeripherals(bondedPeripheralsArray);
// //     });
// // }

// const connect = (item) => {
//     console.log(item.id);
//     BleManager.connect(item.id)
//         .then(() => {
//             // Success code
//             console.log("Connected");
//             setTimeout(() => {
//                 BleManager.retrieveServices(item.id).then((peripheralData) => {
//                     console.log('Retrieved peripheral services', peripheralData);
//                     BleManager.readRSSI(item.id).then((rssi) => {
//                         console.log('Retrieved actual RSSI value', rssi);
//                     });
//                     var characteristics = peripheralData.characteristics
//                     console.log('Retrieved characteristics:', characteristics);
//                 });
//             }, 900);
//         })
//         .catch((error) => {
//             // Failure code
//             console.log(error);
//         });

// }
// return (
//     <View>

//         <Button onPress={() => scanForDevices()} title={"Scan"}>

//         </Button>
//         <View>
//             {/* {
//                 peripherals.map((item, key) => {
//                     <Text>{item.name}</Text>
//                 })
//             } */}
//             {peripherals.map((item, key) => {
//                 return (
//                     // <Text key={key}>{item.name}</Text>
//                     <Button onPress={() => connect(item)} title={item.name.toString()}>

//                     </Button>
//                 )
//             })}
//         </View>

//         {/* <Button onPress={() => connect()} >
//             <Text>Connect</Text>
//         </Button> */}
//     </View >
// );
// };

// export default SecondExample;

import React, {useState, useEffect} from 'react';

import {
  SafeAreaView,
  StyleSheet,
  Image,
  TextStyle,
  ViewStyle,
  ImageStyle,
  ScrollView,
  View,
  Text,
  StatusBar,
  NativeModules,
  NativeEventEmitter,
  Button,
  Platform,
  PermissionsAndroid,
  FlatList,
  TouchableHighlight,
} from 'react-native';

/**
 * Sample BLE React Native App
 *
 * @format
 * @flow strict-local
 */

import BleManager from 'react-native-ble-manager';
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

const FULL = {
  flex: 1,
};
const HEADER = {
  paddingBottom: 12,
  paddingHorizontal: 12,
  paddingTop: 12,
};
const HEADER_TITLE = {
  fontSize: 12,
  fontWeight: 'bold',
  lineHeight: 15,
  textAlign: 'center',
};
const LIST_CONTAINER = {
  alignItems: 'center',
  flexDirection: 'row',
  padding: 10,
};
const IMAGE = {
  borderRadius: 35,
  height: 65,
  width: 65,
};
const LIST_TEXT = {
  marginLeft: 10,
};
const FLAT_LIST = {
  paddingHorizontal: 12,
};

const styles = StyleSheet.create({
  scrollView: {
    //   backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    //   backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    //   color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    //   color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    //   color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

function SecondExample({navigation}) {
  // const { characterStore } = useStores()
  // const { characters } = characterStore

  // useEffect(() => {
  //   async function fetchData() {
  //     await characterStore.getCharacters()
  //   }

  //   fetchData()
  // }, [])

  /****************** Start of BLE methods ***/
  const [isScanning, setIsScanning] = useState(false);
  const peripherals = new Map();
  const [list, setList] = useState([]);

  useEffect(() => {
    // BleManager.start({showAlert: false});
    BleManager.start({showAlert: false, forceLegacy: true}).then(() => {
      // Success code
      console.log('Module initialized');
    });
    bleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      handleDiscoverPeripheral,
    );
    bleManagerEmitter.addListener('BleManagerStopScan', handleStopScan);
    bleManagerEmitter.addListener(
      'BleManagerDisconnectPeripheral',
      handleDisconnectedPeripheral,
    );
    bleManagerEmitter.addListener(
      'BleManagerDidUpdateValueForCharacteristic',
      handleUpdateValueForCharacteristic,
    );

    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      ).then(result => {
        if (result) {
          console.log('Permission is OK');
        } else {
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          ).then(result => {
            if (result) {
              console.log('User accept');
            } else {
              console.log('User refuse');
            }
          });
        }
      });
    }

   
    return () => {
      console.log('unmount');
      bleManagerEmitter.removeListener(
        'BleManagerDiscoverPeripheral',
        handleDiscoverPeripheral,
      );
      bleManagerEmitter.removeListener('BleManagerStopScan', handleStopScan);
      bleManagerEmitter.removeListener(
        'BleManagerDisconnectPeripheral',
        handleDisconnectedPeripheral,
      );
      bleManagerEmitter.removeListener(
        'BleManagerDidUpdateValueForCharacteristic',
        handleUpdateValueForCharacteristic,
      );
    };
  }, []);

  const startScan = () => {
    if (!isScanning) {
      console.log('starting scan');
      setList([]);
      setIsScanning(true);
      BleManager.scan([], 18, true)
        .then(results => {
          console.log('Scanning...', results);
          BleManager.getBondedPeripherals([]).then((bondedPeripheralsArray) => {
            // Each peripheral in returned array will have id and name properties
            console.log("Bonded peripherals: " + JSON.stringify(bondedPeripheralsArray));
          });
        })
        .catch(err => {
          console.error(err);
        });
    }
  };

  const handleStopScan = () => {
    BleManager.getDiscoveredPeripherals([]).then((peripheralsArray) => {
      // Success code
      console.log("Discovered peripherals: " + JSON.stringify(peripheralsArray));
    });
    console.log('Scan is stopped');
    setIsScanning(false);
  };

  const handleDisconnectedPeripheral = data => {
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      setList(Array.from(peripherals.values()));
    }
    console.log('Disconnected from ' + data.peripheral);
  };

  const handleUpdateValueForCharacteristic = data => {
    console.log(
      'Received data from ' +
        data.peripheral +
        ' characteristic ' +
        data.characteristic,
      data.value,
    );
  };

  async function retrieveConnected() {
    console.log('getConnectederp');
    let results = await BleManager.getConnectedPeripherals([]);
    console.log('results = results', results);
    if (results.length == 0) {
      console.log('No connected peripherals');
    }
    console.log(results);
    for (var i = 0; i < results.length; i++) {
      var peripheral = results[i];
      peripheral.connected = true;
      peripherals.set(peripheral.id, peripheral);
      setList(Array.from(peripherals.values()));
    }
  }

  const handleDiscoverPeripheral = peripheral => {
    console.log('Got ble peripheral', peripheral);
    if (!peripheral.name) {
      peripheral.name = 'NO NAME';
    }
    peripherals.set(peripheral.id, peripheral);
    setList(Array.from(peripherals.values()));
  };

  const testPeripheral = peripheral => {
    if (peripheral) {
      console.log('peripheral in the if ', peripheral);
      if (peripheral.connected) {
        console.log('peripheral in the connected if ', peripheral);
        // BleManager.disconnect(peripheral.id);
      } else {
        console.log('peripheral in the else ', peripheral.id);

        setTimeout(() => {
          BleManager.connect(peripheral.id)
            .then(() => {
              let p = peripherals.get(peripheral.id);
              console.log('p in connected under testP ', p);
              if (p) {
                console.log('P in the if before connected set ', p);
                p.connected = true;
                console.log('P in the if after connected set ', p);
                peripherals.set(peripheral.id, p);
                setList(Array.from(peripherals.values()));
              }
              console.log('Connected to ' + peripheral.id);

              setTimeout(() => {
                /* Test read current RSSI value */
                BleManager.retrieveServices(peripheral.id).then(
                  peripheralData => {
                    console.log(
                      'Retrieved peripheral services',
                      peripheralData,
                    );

                    BleManager.readRSSI(peripheral.id).then(rssi => {
                      console.log('Retrieved actual RSSI value', rssi);
                      let p = peripherals.get(peripheral.id);
                      if (p) {
                        p.rssi = rssi;
                        peripherals.set(peripheral.id, p);
                        setList(Array.from(peripherals.values()));
                      }
                    });
                  },
                );

                // Test using bleno's pizza example
                // https://github.com/sandeepmistry/bleno/tree/master/examples/pizza
                /*
            BleManager.retrieveServices(peripheral.id).then((peripheralInfo) => {
              console.log(peripheralInfo);
              var service = '13333333-3333-3333-3333-333333333337';
              var bakeCharacteristic = '13333333-3333-3333-3333-333333330003';
              var crustCharacteristic = '13333333-3333-3333-3333-333333330001';

              setTimeout(() => {
                BleManager.startNotification(peripheral.id, service, bakeCharacteristic).then(() => {
                  console.log('Started notification on ' + peripheral.id);
                  setTimeout(() => {
                    BleManager.write(peripheral.id, service, crustCharacteristic, [0]).then(() => {
                      console.log('Writed NORMAL crust');
                      BleManager.write(peripheral.id, service, bakeCharacteristic, [1,95]).then(() => {
                        console.log('Writed 351 temperature, the pizza should be BAKED');
                        
                        //var PizzaBakeResult = {
                        //  HALF_BAKED: 0,
                        //  BAKED:      1,
                        //  CRISPY:     2,
                        //  BURNT:      3,
                        //  ON_FIRE:    4
                        //};
                      });
                    });

                  }, 500);
                }).catch((error) => {
                  console.log('Notification error', error);
                });
              }, 200);
            });*/
              }, 900);
            })
            .catch(error => {
              console.log('Connection error', error);
            });
        }, 300);
      }
    }
  };

  const renderItem = item => {
    const color = item.connected ? 'green' : '#fff';
    return (
      <TouchableHighlight onPress={() => testPeripheral(item)}>
        <View style={[styles.row, {backgroundColor: color}]}>
          <Text
            style={{
              fontSize: 12,
              textAlign: 'center',
              color: '#333333',
              padding: 10,
            }}>
            {item.name}
          </Text>
          <Text
            style={{
              fontSize: 10,
              textAlign: 'center',
              color: '#333333',
              padding: 2,
            }}>
            RSSI: {item.rssi}
          </Text>
          <Text
            style={{
              fontSize: 8,
              textAlign: 'center',
              color: '#333333',
              padding: 2,
              paddingBottom: 20,
            }}>
            {item.id}
          </Text>
        </View>
      </TouchableHighlight>
    );
  };

  /****************** End of BLE methods ***/
  return (
    <View testID="BLEConnectScreen" style={FULL}>
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>
            <View style={{margin: 10}}>
              <Button
                title={'Scan Bluetooth (' + (isScanning ? 'on' : 'off') + ')'}
                onPress={() => startScan()}
              />
            </View>

            <View style={{margin: 10}}>
              <Button
                title="Retrieve connected peripherals"
                onPress={() => retrieveConnected()}
              />
            </View>

            {list.length == 0 && (
              <View style={{flex: 1, margin: 20}}>
                <Text style={{textAlign: 'center'}}>No peripherals</Text>
              </View>
            )}
          </View>
        </ScrollView>
        <FlatList
          data={list}
          renderItem={({item}) => renderItem(item)}
          keyExtractor={item => item.id}
        />
      </SafeAreaView>
    </View>
  );
}

export default SecondExample;
